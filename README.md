# Detecção de Anomalias Através de Aprendizado de Máquina nas Prestações de Contas das Eleições Municipais Brasileiras de 2016
_Trabalho de Conclusão de Especialização em Ciência de Dados pela PUCRS_

## Resumo
Anomalias são instâncias em descompasso num
conjunto de dados, significativamente diferentes das demais.
Nesse contexto, o presente trabalho é o resultado de um
exercício de __detecção de anomalias nas prestações de contas
das Eleições Municipais de 2016__, que consiste num _dataset_ com
mais de 5 milhões de registros, através da aplicação de técnicas
de aprendizado de máquina. A partir da realização de
experimentos de extração de características e de _clustering_,
considerando diferentes medidas estatísticas, escores foram
definidos para medir o grau de anormalidade de cada
agrupamento de dados e de cada instância, possibilitando a
detecção de anomalias na grafia de descrições, nos valores, nas
datas e na classificação das prestações de contas. Os resultados
dos experimentos demonstram que é possível utilizar
aprendizado de máquina para detectar anomalias nas
prestações de contas das Eleições de 2016.

## Palavras-chave
Detecção de Anomalia, Aprendizado de Máquina, _Clustering_, _K-means_, Extração de Características, _Word Embeddings_, TF-IDF, _Outliers_.

## Artigo
Artigo referente ao trabalho de conclusão da Especialização em Ciência de Dados pela PUCRS: [Artigo Trabalho Final - Esp. Ciência de Dados - Bruno da Silva Lessa.pdf](Artigo Trabalho Final - Esp. Ciência de Dados - Bruno da Silva Lessa.pdf).

## Arquivos adicionais
Devido ao tamanho, o _dataset_ e outros arquivos de dados foram disponibilizados fora do GIT:
- _Dataset_ com as prestações de contas: https://drive.google.com/drive/u/0/folders/1I2hP5VTBZmu2oyVz-4WA5QVtG-gvz5sF.
- Endereço de onde foi obtido o _dataset_: http://www.tse.jus.br/eleicoes/estatisticas/repositorio-de-dados-eleitorais-1/repositorio-de-dados-eleitorais.
- Matrizes geradas pelos experimentos: https://drive.google.com/drive/folders/1I2hP5VTBZmu2oyVz-4WA5QVtG-gvz5sF?usp=sharing.
- Modelos gerados pelos experimentos: https://drive.google.com/drive/folders/1Ro8oPW1wgBfiXXsp4WF4TBWHB9Tkn63T?usp=sharing.
